#!/usr/bin/env python3
from matplotlib import pyplot
from os.path import expanduser
import colorcet
import matplotlib.cm
import matplotlib.colors
import numpy
import scipy.special
import sqlite3

home = expanduser("~")
pyplot.rcParams['text.usetex'] = True
pyplot.rcParams['text.latex.preamble'] = r"\usepackage{amsmath}"
pyplot.rcParams['figure.autolayout'] = True

norm = matplotlib.colors.Normalize(vmin=8, vmax=14)
mapper = matplotlib.cm.ScalarMappable(norm=norm, cmap=colorcet.cm.CET_R2)

renyi = 'I3'
Delta = 1

pyplot.figure(figsize=(4, 3))


pyplot.plot([1/10, 1/12, 1/14], [0.9984266061879483, 0.996525350433225, 0.9960208482729571], '.-', markersize=10, label=r"$\ell = 1$")
pyplot.plot([1/8, 1/10, 1/12, 1/14], [0.9113139676588908, 0.9278151610410657, 0.9436705911785879, 0.9553554802260598], 'x-', markersize=10, label=r"$\ell = 2$")
pyplot.plot([1/8, 1/10, 1/12], [0.8917450734105232, 0.9196330531476922, 0.9430552030071797], '+-', markersize=10, label=r"$\ell = 3$")
pyplot.plot([0,0.13], [1,1], color='black', linewidth=0.5)


pyplot.ylim([0.87, 1.01])
pyplot.xlim([0,0.13])
pyplot.ylabel(r"Asymptotic Value $\langle \tilde I_3(\infty) \rangle$")
pyplot.xlabel(r"Inverse System Size $L^{-1}$")
pyplot.legend(loc='lower left')

filename = f'{renyi}-OBC-Delta{Delta}-butterfly-asymptotic-I3-different-l-finite-size-plot.png'
pyplot.savefig(filename, format='png', dpi=300, transparent=False)
print("Written", filename)
