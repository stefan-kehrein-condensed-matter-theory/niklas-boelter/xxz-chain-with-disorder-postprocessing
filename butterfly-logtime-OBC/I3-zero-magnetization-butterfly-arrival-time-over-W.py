#!/usr/bin/env python3
from matplotlib import pyplot
from os.path import expanduser
import colorcet
import cycler
import numpy
import scipy.special
import sqlite3
import sys
import colorcet
import matplotlib.colors
import matplotlib.cm

home = expanduser("~")

sys.path.insert(0, "..")
from tripartite_information_utils import distance_subsystems

pyplot.rcParams['text.usetex'] = True
pyplot.rcParams['text.latex.preamble'] = r"\usepackage{amsmath}"
pyplot.rcParams['figure.autolayout'] = True
pyplot.rcParams['axes.prop_cycle'] = cycler.cycler('color', colorcet.glasbey_category10)

con = sqlite3.connect(home + "/xxz-chain-with-disorder-data/rocks/xxz-disorder-random-states-OBC.db")
con.enable_load_extension(True)
con.execute("select load_extension('" + home + "/qubit-chain-tfd/libsqlitefunctions.so')")
con.enable_load_extension(False)
c = con.cursor()

# pyplot.rcParams['axes.prop_cycle'] = cycler.cycler('color', ["#d70000", "#8c3cff", "#028800", "#00acc7", "#e7a500", "#ff7fd1", "#6c004f", "#1fff00", "#00009d", "#867068", "#004942", "#4f2a00"])

Delta = 1.0


for renyi in ['I3', 'I3_2']:

    for mask_A in (1, 3, 7):

        map_d_to_color = {}
        next_color = 0

        pyplot.figure(figsize=(4, 3))

        d_legends = []

        if mask_A == 1:
            L = 10
            max_d = L - 2

        if mask_A == 3:
            L = 12
            max_d = L - 4

        if mask_A == 7:
            L = 12
            max_d  = L - 6

        beta = scipy.special.binom(L, L // 2)

        print('max_d=', max_d)

        norm = matplotlib.colors.Normalize(vmin=1, vmax=max_d)
        mapper = matplotlib.cm.ScalarMappable(norm=norm, cmap=colorcet.cm.CET_R2)

        for idx_c in range(L, 1, -1):
            if mask_A == 1:
                mask_C = 2**idx_c

            if mask_A == 3:
                mask_C = 2**(idx_c + 1) + 2**(idx_c + 2)

            if mask_A == 7:
                mask_C = 2**(idx_c + 1) + 2**(idx_c + 2) + 2**(idx_c+3)


            data = numpy.array(list(zip(
                                    *(c.execute(f"""
                            SELECT
                                V,
                                arrival_time_{renyi}_asymptotic,
                                arrival_time_{renyi}_asymptotic_err
                            FROM tripartite_information_fit
                            WHERE
                                L = ?
                                AND mask_A = ?
                                AND mask_C = ?
                                AND lambda = ?
                                AND mu = 0
                                AND beta = ?
                                AND samplesize > 20
                                AND V < 20
                                AND V > 0
                                AND arrival_time_{renyi}_asymptotic_err IS NOT NULL
                            ORDER BY V ASC
                                """, (L, mask_A, mask_C, Delta, beta))))))
            if len(data) != 3:
                continue
            if not idx_c in map_d_to_color:
                # map_d_to_color[idx_c] = 'C' + str(next_color)
                # next_color += 1
                map_d_to_color[idx_c] = mapper.to_rgba(distance_subsystems(mask_C, mask_A))

            if not idx_c in d_legends:
                pyplot.errorbar(float("NaN"), float("NaN"), float("NaN"), capsize=2, label=rf"$d = {int(distance_subsystems(mask_C, mask_A))}$", color=map_d_to_color[idx_c])
                d_legends.append(idx_c)

            print(L, idx_c, len(data[0]))
            W, t, t_err = data
            # if L == 6:
            #     fmt = "v-"
            # if L == 8:
            #     fmt = "<-"
            # if L == 10:
            #     fmt = "x:"
            # if L == 12:
            #     fmt = "*--"
            # if L == 14:
            #     fmt = "o-"
            fmt = '-'

            print('L=', L)
            print('W=', W)
            print('t=', t)
            print('t_err=', t_err)
            pyplot.errorbar(W, 2**t, (2**t - 2**(t - t_err), 2**(t + t_err) - 2**t), fmt=fmt, label=None, capsize=2, markersize=4, linewidth=1, color=map_d_to_color[idx_c])

        # pyplot.plot(float("NaN"), float("NaN"), "v", label=rf"$L = 6$", color="black")
        # pyplot.plot(float("NaN"), float("NaN"), "<", label=rf"$L = 8$", color="black")
        # pyplot.plot(float("NaN"), float("NaN"), "x", label=rf"$L = 10$", color="black")
        # pyplot.plot(float("NaN"), float("NaN"), "*", label=rf"$L = 12$", color="black")
        # pyplot.plot(float("NaN"), float("NaN"), "o", label=rf"$L = 14$", color="black")

        # pyplot.title(r"XXZ Chain with $\Delta = 1.0$, all states with zero magnetization, OBC")
        pyplot.yscale('log')
        # pyplot.xscale('log')
        pyplot.xlabel(r"Disorder Strength $W$")
        if renyi == 'I3':
            pyplot.ylabel(r"Arrival Time $T$")
        if renyi == 'I3_2':
            pyplot.ylabel(r"Arrival Time $T$ $(I_3^2)$")
        if renyi == 'I3_min':
            pyplot.ylabel(r"Arrival Time $T$ $(I_3^\textrm{min})$")

        
        # pyplot.colorbar(mappable=mapper, label="Subsystem Separation $d$")
        pyplot.legend(ncol=2, fontsize='small')
        # pyplot.show()
        filename = f'{renyi}-L{L}-a{mask_A}-OBC-Delta{Delta}-butterfly-arrival-time-over-W.png'
        pyplot.savefig(filename, format='png', dpi=300)  # , transparent=True)
        print("Written", filename)
