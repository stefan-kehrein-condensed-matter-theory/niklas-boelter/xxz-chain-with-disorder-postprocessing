#!/usr/bin/env python3
from matplotlib import pyplot
from os.path import expanduser
import colorcet
import lmfit
import lmfit.models
import matplotlib.cm
import matplotlib.colors
import numpy
import scipy.interpolate
import scipy.special
import sqlite3

home = expanduser("~")
pyplot.rcParams['text.usetex'] = True
pyplot.rcParams['text.latex.preamble'] = r"\usepackage{amsmath}"
pyplot.rcParams['figure.autolayout'] = True

norm = matplotlib.colors.Normalize(vmin=8, vmax=14)
mapper = matplotlib.cm.ScalarMappable(norm=norm, cmap=colorcet.cm.CET_R2)


con = sqlite3.connect(home + "/xxz-chain-with-disorder-data/rocks/xxz-disorder-random-states-OBC.db")
con.enable_load_extension(True)
con.execute("select load_extension('" + home + "/qubit-chain-tfd/libsqlitefunctions.so')")
con.enable_load_extension(False)
c = con.cursor()

def ξBKT(W, L, slope, intercept):
    ΔW = W-(intercept+slope*L)
    sign = numpy.full(len(ΔW), 1.0, dtype=numpy.float64)
    sign[ΔW<0] = -1.0
    b = numpy.full(len(ΔW), 2.23, dtype=numpy.float64)
    b[ΔW<0] = 2.23
    result = sign*numpy.exp(b/numpy.sqrt(sign*ΔW))
    print(L, ΔW, sign, result) 

    return result

for renyi in ['I3', 'I3_2']:
    for mask_A in (1, 3, 7):
        Delta = 1.0

        pyplot.figure(figsize=(4, 3))


        L_data = []
        Wcrit_data = []

        for L in (8, 10, 12, 14):
            idx_c = L - 1
            if mask_A == 1:
                mask_C = 2**idx_c
            if mask_A == 3:
                mask_C = 2**idx_c + 2**(idx_c - 1)
            if mask_A == 7:
                mask_C = 2**idx_c + 2**(idx_c - 1) + 2**(idx_c - 2)

            data = numpy.array(list(zip(
                *(c.execute(f"""
                            SELECT
                                V,
                                {renyi}_asymptotic,
                                {renyi}_asymptotic_err
                            FROM tripartite_information_fit
                            WHERE
                                L = ?
                                AND mask_A = ?
                                AND mask_C = ?
                                AND lambda = ?
                                AND mu = 0
                                AND beta = ?
                                AND V < 11
                            ORDER BY V ASC
                                """, (L, mask_A, mask_C, Delta, scipy.special.binom(L, L // 2)))))))
            if len(data) != 3:
                continue
            W, I3, I3_err = data

            I3_err[I3_err == None] = 0.0


            ipf = scipy.interpolate.splrep(W, I3-0.5)
            L_data.append(L)
            Wcrit_data.append(scipy.interpolate.sproot(ipf)[0])

        params = lmfit.Parameters()
        params.add('slope', value=1.0)
        params.add('intercept', value=0.0)
        model = lmfit.models.LinearModel()
        out = model.fit(Wcrit_data, params, x=L_data)
        print(renyi, mask_A, out.params)
        slope = float(out.params['slope'].value)
        intercept = float(out.params['intercept'].value)

        for L in (8, 10, 12, 14):
            idx_c = L - 1
            if mask_A == 1:
                mask_C = 2**idx_c
            if mask_A == 3:
                mask_C = 2**idx_c + 2**(idx_c - 1)
            if mask_A == 7:
                mask_C = 2**idx_c + 2**(idx_c - 1) + 2**(idx_c - 2)

            data = numpy.array(list(zip(
                *(c.execute(f"""
                            SELECT
                                V,
                                {renyi}_asymptotic,
                                {renyi}_asymptotic_err
                            FROM tripartite_information_fit
                            WHERE
                                L = ?
                                AND mask_A = ?
                                AND mask_C = ?
                                AND lambda = ?
                                AND mu = 0
                                AND beta = ?
                                AND V < 11
                            ORDER BY V ASC
                                """, (L, mask_A, mask_C, Delta, scipy.special.binom(L, L // 2)))))))
            if len(data) != 3:
                continue
            # print(L, idx_c, len(data[0]))
            W, I3, I3_err = data
            if renyi == "I3":
                print("I3_MAX", L, idx_c, numpy.max(I3))

            I3_err[I3_err == None] = 0.0

            if L == 6:
                fmt = "^-"
            if L == 8:
                fmt = "P-"
            if L == 10:
                fmt = "x-"
            if L == 12:
                fmt = "*-"
            if L == 14:
                fmt = ".-"
            pyplot.errorbar(L/ξBKT(W.astype(numpy.float64), L, slope, intercept), I3, I3_err, fmt=fmt, label=rf"$L = {L}$", capsize=2, barsabove=False, markersize=5, color=mapper.to_rgba(L))

        if mask_A == 3 and renyi == 'I3':
            pyplot.errorbar(16/ξBKT(numpy.array([2.0, 2.5, 3.0, 3.5], dtype=numpy.float64), 16, slope, intercept), [0.604685, 0.458247, 0.308452, 0.250354], [0.0252235, 0.0330931, 0.0215032, 0.0143082], fmt='o:', capsize=2, markersize=3.0, label="$L = 16$", color='black', zorder=2.5)
        pyplot.axhline(y = 0.5, color='grey')
        pyplot.axvline(x = 0.0, color='grey')
        # pyplot.title(r"XXZ Chain with $L = 12, \Delta = 1.0$, A = [1], OBC")
        # pyplot.yscale('log')
        # pyplot.xscale('log')
        # pyplot.ylim([0,1])
        pyplot.xlabel(r"$L/\xi_\mathrm{BKT}$")
        if renyi == 'I3':
            pyplot.ylabel(r"Asymptotic Value $\langle \tilde I_3(\infty) \rangle$")
        if renyi == 'I3_2':
            pyplot.ylabel(r"Asymptotic Value $\langle \tilde I_3^2(\infty) \rangle$")
        if renyi == 'I3_min':
            pyplot.ylabel(r"Asymptotic Value $\langle \tilde I_3^\textrm{min}(\infty) \rangle$")
        pyplot.legend()
        # pyplot.show()
        filename = f'{renyi}-a{mask_A}-OBC-Delta{Delta}-butterfly-asymptotic-I3-different-L-BKT.png'
        pyplot.savefig(filename, format='png', dpi=300, transparent=False)
        print("Written", filename)
