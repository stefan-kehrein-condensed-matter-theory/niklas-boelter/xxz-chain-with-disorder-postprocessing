#!/usr/bin/env python3
# -*- coding: utf8 -*-
from os.path import expanduser
import colorcet
import lmfit
import lmfit.models
import matplotlib.cm
import matplotlib.colors
import matplotlib.pyplot as pyplot
import numpy
import scipy.special
import sqlite3


norm = matplotlib.colors.Normalize(vmin=0, vmax=10)
mapper = matplotlib.cm.ScalarMappable(norm=norm, cmap=colorcet.cm.CET_R2)

pyplot.rcParams['text.usetex'] = True
pyplot.rcParams['text.latex.preamble'] = r"\usepackage{amsmath}"
pyplot.rcParams['figure.autolayout'] = True
home = expanduser("~")

for renyi in ['I3', 'I3_2', 'I3_min']:

    π = numpy.pi

    for mask_A in (1, 3, 7):
        if mask_A == 1:
            normalize = True
        else:
            normalize = False
        for L in [8, 10, 12, 14]:
            beta = scipy.special.binom(L, L // 2)
            con = sqlite3.connect(home + "/xxz-chain-with-disorder-data/rocks/xxz-disorder-random-states-OBC.db")
            # con = sqlite3.connect(home + "/loki/xxz-disorder-random-states-OBC-average.db")
            con.enable_load_extension(True)
            con.execute("select load_extension('" + home + "/qubit-chain-tfd/libsqlitefunctions.so')")
            con.enable_load_extension(False)
            c = con.cursor()

            c.execute("""
                CREATE TABLE IF NOT EXISTS tripartite_information_fit (
                    L integer,
                    mask_A integer,
                    mask_C integer,
                    lambda real,
                    V real,
                    mu real,
                    beta real,
                    I3_asymptotic,
                    I3_asymptotic_err,
                    I3_2_asymptotic,
                    I3_2_asymptotic_err,
                    I3_min_asymptotic,
                    I3_min_asymptotic_err,
                    arrival_time_I3_asymptotic,
                    arrival_time_I3_asymptotic_err,
                    arrival_time_I3_2_asymptotic,
                    arrival_time_I3_2_asymptotic_err,
                    arrival_time_I3_min_asymptotic,
                    arrival_time_I3_min_asymptotic_err,
                    samplesize integer,
                    CONSTRAINT results_average_uniq UNIQUE (L, mask_A, mask_C, lambda, V, mu, beta));""")

            framenumber = 1

            frame_parameters = []

            c.execute("DROP TABLE IF EXISTS haar_averages;")

            with open(home + '/xxz-chain-with-disorder-data/haar_averages.sql', 'r') as file:
                haar_averages_sql = file.read()

                c.executescript(haar_averages_sql)

            found_Haar_average = False
            for result in c.execute(f"SELECT {renyi}, {renyi}_std FROM haar_averages WHERE N = ? AND l = ? AND beta = ? and mu = ? and Delta = ?",
                                    (L, bin(mask_A).count('1'), beta, 0.0, 1.0)):
                I3_Haar = result[0]
                found_Haar_average = True
            if not found_Haar_average:
                print(f"Mising Haar average: L = {L}, l = {bin(mask_A).count('1')}, Delta = 1.0")
                # exit()

            for Delta in numpy.linspace(-4, 4, 17):
                # if Delta != 1.0:
                #     continue

                if mask_A == 1:
                    distinct_mask_C = [2**(L - 1)] + [(2**c) for c in range(2, L)]

                if mask_A == 3:
                    distinct_mask_C = [2**(L - 1) + 2**(L - 2)] + [(2**c + 2**(c + 1)) for c in range(2, L - 1)]

                if mask_A == 7:
                    distinct_mask_C = [2**(L - 1) + 2**(L - 2) + 2**(L - 3)] + [(2**c + 2**(c + 1) + 2**(c+2)) for c in range(2, L - 2)]

                distinct_W = numpy.array(list(zip(
                    *(c.execute("""
                        SELECT DISTINCT V as W
                        FROM results_average
                        WHERE
                            L = ?
                            AND mask_A = ?
                            AND beta = ?
                            AND mu = 0.0
                            AND lambda = ?
                            AND t > 50
                            AND V < 11
                        ORDER BY V ASC""", (L, mask_A, beta, Delta))))))

                if len(distinct_W) == 0:
                    continue

                distinct_W = distinct_W[0]

                # fig, ax = pyplot.subplots()

                # axT = ax.twiny()

                # ax.set_xscale("log")

                # ax.set_prop_cycle('color',pyplot.cm.Spectral(numpy.linspace(0,1,30)))

                def init():
                    fig = pyplot.figure(figsize=(4, 3))

                    # pyplot.ylim([-0.05, 1.15])
                    pyplot.xlim([1, 1.2E12])
                    pl, = pyplot.plot(0, 0, color='white')
                    return pl,

                def update(frame):
                    none_count = 0

                    # ax.cla()

                    my_plots = []

                    # print(distinct_W[0], frame)
                    mask_C = distinct_mask_C[int(frame)]

                    halfway_times_W = []
                    halfway_times_t = []
                    halfway_times_I3 = []

                    print(f'##### a = {mask_A}, c = {mask_C}, Delta = {Delta}, L = {L} #####')

                    for W in distinct_W:
                        data = numpy.array(list(zip(
                            *(c.execute(f"""
                                SELECT
                                    t,
                                    {renyi},
                                    {renyi}_std/sqrt(samplesize),
                                    samplesize
                                FROM results_average
                                WHERE
                                    L = ?
                                    AND mask_A = ?
                                    AND mask_C = ?
                                    AND beta = ?
                                    AND mu = 0.0
                                    AND lambda = ?
                                    AND V = ?
                                    AND (samplesize > 60 OR (samplesize > 0 AND L > 12))
                                    AND t IN (1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024,
                                        2048, 4096, 8192, 16384, 32768, 65536, 131072, 262144,
                                        524288, 1048576, 2097152, 4194304, 8388608, 16777216,
                                        33554432, 67108864, 134217728, 268435456, 536870912,
                                        1073741824, 2147483648, 4294967296, 8589934592,
                                        17179869184, 34359738368, 68719476736, 137438953472,
                                        274877906944, 549755813888, 1099511627776)
                                GROUP BY t
                                ORDER BY t ASC""", (L, mask_A, mask_C, beta, Delta, W))))))

                        if len(data) == 0:
                            continue

                        if max(data[0]) < 1E10:
                            continue

                        # print(f'    W = {W}, samplesize >= {min(data[3])}')

                        logtime = numpy.log2(data[0])
                        I3 = (data[1] - data[1][0]) / (I3_Haar - data[1][0])
                        I3_err = data[2] / (I3_Haar - data[1][0])

                        x = numpy.logspace(0, 63, 1000, base=2.0)

                        params = lmfit.Parameters()
                        params.add('amplitude', value=1.0, min=0.0)
                        params.add('sigma', value=0.1, min=0.0)
                        params.add('center', value=0.5, min=0.0)

                        if mask_A == 1 and mask_C == 128 and L == 10 and W == 1.0:
                            params['sigma'].set(1.0)
                        if mask_A == 3 and mask_C == 768 and L == 12 and W == 1.0:
                            params['sigma'].set(1.0)
                        if mask_A == 1 and mask_C == 2 and L == 10:
                            params['amplitude'].set(2.0)

                        model = lmfit.models.StepModel(form='logistic')
                        out = model.fit([0.0, 0.0, 0.0, 0.0, 0.0] + list(I3), params, x=([-5, -4, -3, -2, -1] + list(logtime)), method="leastsq")
                        # center_minus = out.params['center'].value
                        # amplitude_minus = out.params['amplitude'].value
                        # out.fit(I3+I3_err, x=logtime, method="leastsq")
                        # center_plus = out.params['center'].value
                        # amplitude_plus = out.params['amplitude'].value
                        # out.fit(I3, x=logtime, method="leastsq")
                        # print(out.params['amplitude'].value, out.params['center'].value, out.params['sigma'].value)
                        # print(out.params['amplitude'].stderr, out.params['center'].stderr)

                        if None in [out.params['amplitude'].stderr, out.params['center'].stderr]:
                            out.fit([0.0, 0.0, 0.0, 0.0, 0.0] + list(I3), x=([-5, -4, -3, -2, -1] + list(logtime)), method="least_squares")

                            # print(out.params['amplitude'].stderr, out.params['center'].stderr)

                        if None in [out.params['amplitude'].stderr, out.params['center'].stderr]:
                            none_count = none_count + 1
                            print(W, out.params)

                        # print(out.fit_report())
                        # out.conf_interval(sigmas=[1])
                        # print(out.ci_out)
                        # print(data)
                        # out.fit(I3, weights=1 / numpy.clip(I3_err, 1E-10, 1E+10))

                        normalization_factor = out.params['amplitude'].value

                        upsert_data = {
                            'N': L,
                            'mask_A': mask_A,
                            'mask_C': mask_C,
                            'Delta': Delta,
                            'W': W,
                            'mu': 0.0,
                            'beta': beta,
                            'samplesize': numpy.min(data[3]),
                            'I3_asymptotic': out.params['amplitude'].value,
                            'I3_asymptotic_err': out.params['amplitude'].stderr,
                            'T': out.params['center'].value,
                            'T_err': out.params['center'].stderr
                        }

                        c.execute(f"""
                            INSERT INTO tripartite_information_fit (
                                L,
                                mask_A,
                                mask_C,
                                lambda,
                                V,
                                mu,
                                beta,
                                samplesize,
                                {renyi}_asymptotic,
                                {renyi}_asymptotic_err,
                                arrival_time_{renyi}_asymptotic,
                                arrival_time_{renyi}_asymptotic_err
                            )
                            VALUES (
                                :N,
                                :mask_A,
                                :mask_C,
                                :Delta,
                                :W,
                                :mu,
                                :beta,
                                :samplesize,
                                :I3_asymptotic,
                                :I3_asymptotic_err,
                                :T,
                                :T_err
                            )
                            ON CONFLICT (
                                L,
                                mask_A,
                                mask_C,
                                lambda,
                                V,
                                mu,
                                beta
                            )
                            DO UPDATE SET
                                samplesize = :samplesize,
                                {renyi}_asymptotic = :I3_asymptotic,
                                {renyi}_asymptotic_err = :I3_asymptotic_err,
                                arrival_time_{renyi}_asymptotic = :T,
                                arrival_time_{renyi}_asymptotic_err = :T_err
                            """, upsert_data)

                        if normalize:
                            out.fit([0.0, 0.0, 0.0, 0.0, 0.0] + list(I3 / out.params['amplitude'].value))

                        if W not in [0.5, 1.5, 2.5, 3.5, 4.5, 5.5, 7.0, 9.0]:
                            pyplot.plot(x, out.eval(x=numpy.log2(x)), '--', linewidth=1.0, color='grey')

                        halfway_times_W.append(W)
                        halfway_times_t.append(out.params['center'].value)
                        halfway_times_I3.append(out.eval(x=out.params['center'].value))

                        if normalize:
                            I3 = I3 / normalization_factor
                            I3_err = I3_err / normalization_factor

                        if W not in [0.5, 1.5, 2.5, 3.5, 4.5, 5.5, 7.0, 9.0]:
                            ln = pyplot.errorbar(data[0], I3, I3_err, fmt="o", label=rf"$W$ = {int(W)}", capsize=2, markersize=2.5, barsabove=False, color=mapper.to_rgba(W))
                            for ln2 in ln[1]:
                                my_plots.append(ln2)
                    # if normalize:
                    #     pyplot.plot(2.0**numpy.array(halfway_times_t), numpy.array(halfway_times_I3), "-", color="black")
                    # else:
                    #     pyplot.plot(2.0**numpy.array(halfway_times_t), numpy.array(halfway_times_I3), "x-", color="black")

                    if len(halfway_times_t) == 0:
                        print(f'No data')
                        return []

                #

                    # pyplot.title(rf"XXZ Chain with $L = {L}, \Delta = {Delta}, c = {mask_C}, n = {int(beta)}$")
                    # pyplot.ylim([-0.05, 1.0])
                    # pyplot.xlim([0, 20])
                    # if Delta == 1.0:
                    #     pyplot.legend(ncol=3, loc=(0.25, 0.52))
                    # else:
                    if mask_A == 1:
                        pyplot.legend(ncol=2, loc='lower right', fontsize='x-small')
                    elif mask_A == 3:
                        pyplot.legend(ncol=3, loc=(0.20, 0.61), fontsize='x-small')  # , loc='lower right')
                    elif mask_A == 7:
                        pyplot.legend(ncol=3, loc=(0.20, 0.69), fontsize='x-small')  # , loc='lower right')

                    # pyplot.ylabel(r"Average Tripartite Information $\langle I_3(t) \rangle_\textrm{dis}$ $[I_3^{\textrm{Haar}}]$")
                    if normalize:
                        if renyi == "I3":
                            pyplot.ylabel(r"Normalized Tripartite Information $\langle \tilde I_3(t)\rangle / \Lambda(W) $")
                        if renyi == "I3_2":
                            pyplot.ylabel(r"Normalized Tripartite Information $\langle \tilde I_3^{(2)}(t)\rangle / \Lambda(W) $")
                        if renyi == "I3_min":
                            pyplot.ylabel(r"Normalized Tripartite Information $\langle \tilde I_3^{\textrm{min}}(t)-I_3(0)\rangle / \Lambda(W) $")
                    else:
                        if renyi == "I3":
                            pyplot.ylabel(r"Rescaled Tripartite Information $\langle \tilde I_3(t)\rangle$")
                        if renyi == "I3_2":
                            pyplot.ylabel(r"Rescaled Tripartite Information $\langle \tilde I_3^{(2)}(t)\rangle$")
                        if renyi == "I3_min":
                            pyplot.ylabel(r"Rescaled Tripartite Information $\langle \tilde I_3^{\textrm{min}}(t)\rangle$")

                    pyplot.xlabel(r"Time $t$")
                    pyplot.xscale('log')

                    axBottom = pyplot.gca()
                    axTop = axBottom.twiny()

                    axTop.set_xscale('log')
                    # axTop.set_yscale('log')

                    axTop.set_xticks([2.0**0, 2.0**10, 2.0**20, 2.0**30, 2.0**40])
                    axTop.set_xticklabels(["$2^0$", "$2^{10}$", "$2^{20}$", "$2^{30}$", "$2^{40}$"])
                    axBottom.set_xticks([1, 1E3, 1E6, 1E9, 1E12])
                    axTop.set_xlim(axBottom.get_xlim())
                    # pyplot.show()

                    # print(my_plots)

                    # ln = pyplot.plot([4.5E13, 4.5E13],[0.0, 1.0], '--', color='red')
                    # my_plots.append(ln[0])

                    print('none_count', none_count)

                    return my_plots,

                frames = numpy.linspace(0, len(distinct_mask_C) - 1, len(distinct_mask_C))

                # FFwriter = FFMpegWriter(fps=1)
                # ani = FuncAnimation(fig, update, frames=frames, init_func=init, blit=False)
                # pyplot.show()
                # ani.save('example.mp4', writer=FFwriter)
                # print(ani.to_html5_video())

                for frame in frames:
                    init()
                    if len(update(frame)) == 0:
                        continue
                    filename = f'{renyi}-L{L}-a{mask_A}-OBC-Delta{Delta}-c{distinct_mask_C[int(frame)]}-butterfly-logtime.png'
                    pyplot.savefig(filename, format='png', dpi=300, transparent=False)
                    print("Written", filename)
                    print()
                    print()
                    pyplot.close('all')
            con.commit()
            con.close()
