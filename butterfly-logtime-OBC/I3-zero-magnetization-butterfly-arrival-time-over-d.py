#!/usr/bin/env python3
from matplotlib import pyplot
from os.path import expanduser
import math
import numpy
import scipy.special
import sqlite3
import sys
import colorcet
import matplotlib.colors
import matplotlib.cm

home = expanduser("~")

sys.path.insert(0, "..")
from tripartite_information_utils import distance_subsystems

pyplot.rcParams['text.usetex'] = True
pyplot.rcParams['text.latex.preamble'] = r"\usepackage{amsmath}"
pyplot.rcParams['figure.autolayout'] = True

con = sqlite3.connect(home + "/xxz-chain-with-disorder-data/rocks/xxz-disorder-random-states-OBC.db")
con.enable_load_extension(True)
con.execute("select load_extension('" + home + "/qubit-chain-tfd/libsqlitefunctions.so')")
con.enable_load_extension(False)
c = con.cursor()

for renyi in ['I3', 'I3_2']:

    Delta = 1.0

    norm = matplotlib.colors.Normalize(vmin=0, vmax=10)
    mapper = matplotlib.cm.ScalarMappable(norm=norm, cmap=colorcet.cm.CET_R2)

    for mask_A in (1, 3, 7):
        pyplot.figure(figsize=(4, 3))

        map_W_to_color = {}
        next_color = 0

        for L in (12, 10, 8):
            # for W in [0.5, 1.0, 1.5, 2.0, 2.5, 3.0, 3.5, 4.0, 4.5, 5.0, 5.5, 6.0, 7.0, 8.0, 9.0, 10.0]:
            beta = scipy.special.binom(L, L // 2)

            for W in [10.0, 7.0, 4.0, 1.0]:
                if W not in map_W_to_color:
                    map_W_to_color[W] = mapper.to_rgba(W)  # "C" + str(next_color)
                    next_color = next_color + 1
                    pyplot.plot([], [], 's', label=rf"$W = {int(W)}$", color=map_W_to_color[W])

                data = numpy.array(list(zip(
                                        *(c.execute(f"""
                                SELECT
                                    mask_C,
                                    arrival_time_{renyi}_asymptotic,
                                    arrival_time_{renyi}_asymptotic_err
                                FROM tripartite_information_fit
                                WHERE
                                    L = ?
                                    AND mask_A = ?
                                    AND (mask_A == 1 AND mask_C > 2 OR mask_A == 3 AND mask_C > 12 OR mask_A == 7 AND mask_C > 56)
                                    AND V = ?
                                    AND lambda = ?
                                    AND mu = 0
                                    AND beta = ?
                                    AND samplesize > 8
                                    AND arrival_time_{renyi}_asymptotic_err NOT NULL
                                ORDER BY V ASC
                                    """, (L, mask_A, W, Delta, scipy.special.binom(L, L // 2)))))))
                if len(data) != 3:
                    continue
                print(L, W, len(data[0]))
                mask_C, t, t_err = data
                mask_C = mask_C.astype(int)

                if L == 6:
                    fmt = "^-b"
                if L == 8:
                    fmt = "P:"
                if L == 10:
                    fmt = "x-."
                if L == 12:
                    fmt = "*--"
                if L == 14:
                    fmt = "o-"

                print(distance_subsystems(mask_C, mask_A))
                pyplot.errorbar(distance_subsystems(mask_C, mask_A), 2**t, (2**t - 2**(t-t_err), 2**(t+t_err) - 2**t), fmt=fmt, capsize=2, barsabove=False, markersize=4, linewidth=1, color=map_W_to_color[W])

        # pyplot.plot(float("NaN"), float("NaN"), "o-", label=rf"$L = 14$", color="black")
        pyplot.plot(float("NaN"), float("NaN"), "*--", label=rf"$L = 12$", color="black")
        pyplot.plot(float("NaN"), float("NaN"), "x-.", label=rf"$L = 10$", color="black")
        pyplot.plot(float("NaN"), float("NaN"), "P:", label=rf"$L = 8$", color="black")
        # pyplot.plot(float("NaN"), float("NaN"), "^", label=rf"$L = 6$", color="black")

        # pyplot.title(r"XXZ Chain with $\Delta = 1.0$, all states with zero magnetization, OBC")
        pyplot.yscale('log')
        # pyplot.colorbar(mappable=mapper, label="Disorder Strength $W$")
        # pyplot.xscale('log')
        # pyplot.xlabel(r"Disorder Strength $W$")
        pyplot.xlabel(r"Distance Between Subsystems $d$")
        if renyi == 'I3':
            pyplot.ylabel(r"Arrival Time $T$")
        if renyi == 'I3_2':
            pyplot.ylabel(r"Arrival Time $T$ $(I_3^2)$")
        if renyi == 'I3_min':
            pyplot.ylabel(r"Arrival Time $T$ $(I_3^\textrm{min})$")

        pyplot.legend(ncol=2, fontsize='small')
        # pyplot.show()
        filename = f'{renyi}-a{mask_A}-OBC-Delta{Delta}-butterfly-arrival-time-over-d.png'
        pyplot.savefig(filename, format='png', dpi=300)  # , transparent=True)
        print("Written", filename)
