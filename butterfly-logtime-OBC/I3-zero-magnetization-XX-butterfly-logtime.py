#!/usr/bin/env python3
# -*- coding: utf8 -*-
from os.path import expanduser
import colorcet
import lmfit
import lmfit.models
import matplotlib.cm
import matplotlib.colors
import matplotlib.pyplot as pyplot
import numpy
import scipy.special
import scipy.ndimage.filters
import sqlite3

norm = matplotlib.colors.Normalize(vmin=0, vmax=10)
mapper = matplotlib.cm.ScalarMappable(norm=norm, cmap=colorcet.cm.CET_R2)

pyplot.rcParams['text.usetex'] = True
pyplot.rcParams['text.latex.preamble'] = r"\usepackage{amsmath}"
pyplot.rcParams['figure.autolayout'] = True

home = expanduser("~")

L = 12

con = sqlite3.connect(home + "/xxz-chain-with-disorder-data/tilly/xxz-disorder-random-states-OBC.db")
con.enable_load_extension(True)
con.execute("select load_extension('" + home + "/xxz-chain-with-disorder-postprocessing/libsqlitefunctions')")
con.execute("select load_extension('" + home + "/xxz-chain-with-disorder-postprocessing/popcount')")
con.enable_load_extension(False)
c = con.cursor()

renyi = 'I3'
mask_A = 0b11
mask_C = 2**(L-1) + 2**(L-2)
beta = scipy.special.binom(L, L // 2)
Delta = 0.0 # !!!!!!!!
W = 10
I3_Haar = -1.8540843304161


with open(home + '/xxz-chain-with-disorder-data/subsystem_entropies.sql', 'r') as file:
    subsystem_entropies_sql = file.read()

    c.executescript(subsystem_entropies_sql)

c.execute("CREATE TABLE IF NOT EXISTS results_average (L integer, mask_A integer, mask_C integer, lambda real, V real, mu real, beta real, t real, I3 real, I3_std real, I3_2 real, I3_2_std real, I3_min real, I3_min_std real, runtime real, zero_magnetization_ipr real, zero_magnetization_ipr_std real,  zero_magnetization_participation_Shannon_entropy real, zero_magnetization_participation_Shannon_entropy_std real, zero_magnetization_participation_collision_entropy real, zero_magnetization_participation_collision_entropy_std real, zero_magnetization_participation_min_entropy real, zero_magnetization_participation_min_entropy_std real, consecutive_level_spacing_ratio real, consecutive_level_spacing_ratio_std real, samplesize integer, CONSTRAINT results_average_uniq UNIQUE (L, mask_A, mask_C, lambda, V, mu, beta, t));")
c.execute("REPLACE INTO results_average SELECT r.L, r.mask_A, r.mask_C, r.lambda, r.V, r.mu, r.beta, r.t, A.S + B.S + C.S + D.S - avg(AB.S + r.S_AC + r.S_AD), stdev(r.S_AC + r.S_AD), A.S2 + B.S2 + C.S2 + D.S2 - avg(AB.S2 + r.S2_AC + r.S2_AD), stdev(r.S2_AC + r.S2_AD), A.Smin + B.Smin + C.Smin + D.Smin - avg(AB.Smin + r.Smin_AC + r.Smin_AD), stdev(r.Smin_AC + r.Smin_AD), avg(r.runtime), avg(r.zero_magnetization_ipr), stdev(r.zero_magnetization_ipr), avg(r.zero_magnetization_participation_Shannon_entropy), stdev(r.zero_magnetization_participation_Shannon_entropy), avg(r.zero_magnetization_participation_collision_entropy), stdev(r.zero_magnetization_participation_collision_entropy), avg(r.zero_magnetization_participation_min_entropy), stdev(r.zero_magnetization_participation_min_entropy), avg(r.consecutive_level_spacing_ratio), stdev(r.consecutive_level_spacing_ratio), COUNT(*) FROM results_AC_AD_ac27935 AS r, subsystem_entropies AS A, subsystem_entropies AS B, subsystem_entropies AS C, subsystem_entropies AS D, subsystem_entropies AS AB WHERE A.N = r.L AND A.l = popcount(r.mask_A) AND B.N = r.L AND B.l = (r.L - popcount(r.mask_A)) AND C.N = r.L AND C.l = popcount(r.mask_C) AND D.N = r.L AND D.l = (r.L - popcount(r.mask_C)) AND AB.N = r.L AND AB.l = r.L GROUP BY r.L, r.mask_A, r.mask_C, r.lambda, r.V, r.mu, r.beta, r.t;")

fig = pyplot.figure(figsize=(4,3))
pyplot.xlabel(r"Time $t$")
pyplot.ylabel(r"Rescaled Tripartite Information $\langle \tilde I_3(t)\rangle$")
pyplot.xscale('log')
axBottom = pyplot.gca()
axTop = axBottom.twiny()

pyplot.ylim([1E-12,1])
pyplot.xlim([1, 1.2E12])

axTop.set_xscale('log')
# axTop.set_yscale('log')

axTop.set_xticks([2.0**0, 2.0**10, 2.0**20, 2.0**30, 2.0**40])
axTop.set_xticklabels(["$2^0$", "$2^{10}$", "$2^{20}$", "$2^{30}$", "$2^{40}$"])
axBottom.set_xticks([1, 1E3, 1E6, 1E9, 1E12])

axTop.set_xlim([1,1.2E12])
axBottom.set_xlim([1,1.2E12])


i = 0

for W in [0.5, 1, 2, 3, 4]:
    data_avg = numpy.array(list(zip(
        *(c.execute(f"""
            SELECT
                r.V,
                avg(r.{renyi}-r0.{renyi})/({I3_Haar}-r0.{renyi}),
                stdev(r.{renyi}_std/sqrt(r.samplesize))/({I3_Haar}-r0.{renyi}),
                min(r.samplesize)
            FROM results_average r, results_average r0
            WHERE
                r.L = ? AND r0.L = r.L
                AND r.mask_A = ? AND r0.mask_A = r.mask_A
                AND r.mask_C = ? AND r0.mask_C = r.mask_C
                AND r.beta = ? AND r0.beta = r.beta
                AND r.mu = 0.0 AND r0.mu = r.mu
                AND r.lambda = ? AND r0.lambda = r.lambda
                AND r.t > 1E3
                AND r.t < 1E10
                AND r0.t = 1.0
                AND r.V = r0.V
                AND r.V = ?
            GROUP BY r.V
            ORDER BY r.V ASC""", (L, mask_A, mask_C, beta, Delta, W))))))

    print(data_avg[1])

    data = numpy.array(list(zip(
        *(c.execute(f"""
            SELECT
                t,
                {renyi},
                {renyi}_std/sqrt(samplesize),
                samplesize
            FROM results_average
            WHERE
                L = ?
                AND mask_A = ?
                AND mask_C = ?
                AND beta = ?
                AND mu = 0.0
                AND lambda = ?
                AND V = ?
                AND t < 1099511627777
            GROUP BY t
            ORDER BY t ASC""", (L, mask_A, mask_C, beta, Delta, W))))))
    if len(data) != 4:
        continue
    I3 = (data[1] - data[1][0]) / (I3_Haar - data[1][0]) 
    I3_err = data[2] / (I3_Haar - data[1][0])

    print(f"W = {W}, samplesize >= {min(data[3])}")

    pyplot.plot(data[0], scipy.ndimage.filters.gaussian_filter1d(I3, 5), '-', label=rf"$ W = {W}$", color = mapper.to_rgba(W))
    pyplot.plot(data[0], I3, 'o-', color = mapper.to_rgba(W), alpha=0.5, markersize=1, linewidth=1)
    i = i+1

pyplot.legend(ncol=1, fontsize='x-small')




data_avg = numpy.array(list(zip(
        *(c.execute(f"""
            SELECT
                r.V,
                avg(r.{renyi}-r0.{renyi})/({I3_Haar}-r0.{renyi}),
                stdev(r.{renyi}_std/sqrt(r.samplesize))/({I3_Haar}-r0.{renyi}),
                min(r.samplesize)
            FROM results_average r, results_average r0
            WHERE
                r.L = ? AND r0.L = r.L
                AND r.mask_A = ? AND r0.mask_A = r.mask_A
                AND r.mask_C = ? AND r0.mask_C = r.mask_C
                AND r.beta = ? AND r0.beta = r.beta
                AND r.mu = 0.0 AND r0.mu = r.mu
                AND r.lambda = ? AND r0.lambda = r.lambda
                AND r.t > 1E3
                AND r.t < 1E10
                AND r0.t = 1.0
                AND r.V = r0.V
            GROUP BY r.V
            ORDER BY r.V ASC""", (L, mask_A, mask_C, beta, Delta))))))

print(data_avg)


ax2 = fig.add_axes([0.29, 0.55, 0.35, 0.30])
ax2.plot(data_avg[0], data_avg[1], '-', linewidth=0.5, color='black')

for i in range(0, len(data_avg[0])):
    ax2.errorbar(data_avg[0][i], data_avg[1][i], data_avg[2][i], capsize=2, capthick=0.5, linewidth=0.5, markersize=1.5, fmt='o', color=mapper.to_rgba(data_avg[0][i]))
ax2.set_xlabel(r"Interaction Strength $W$", fontsize='x-small')
ax2.set_ylabel(r"$\langle \tilde I_3\rangle_\textrm{Plateau}$", fontsize='small')
ax2.set_yticks([0,0.2,0.4])
ax2.set_xticks([0,5,10])




pyplot.tight_layout()
#pyplot.show()
filename = f'{renyi}-L{L}-a{mask_A}-OBC-Delta{Delta}-c{mask_C}-butterfly-logtime.png'
pyplot.savefig(filename, format='png', dpi=300, transparent=False)
print("Written", filename)
