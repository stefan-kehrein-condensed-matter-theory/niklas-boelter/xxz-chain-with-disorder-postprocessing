#!/usr/bin/env fish
if test (count $argv) -ne 2;
    echo "Usage:" (status filename) "number_of_spins boundary_conditions" 1>&2;
    exit -1
end
set number_of_spins $argv[1]
set boundary_conditions $argv[2]

echo ".load $HOME/qubit-chain-tfd/cksumvfs.so"
echo ".open /scratch/niklas.boelter/rocks-rawdata/xxz-disorder-random-states-$boundary_conditions-N$number_of_spins.db"
echo ".load $HOME/qubit-chain-tfd/libsqlitefunctions.so"
echo 'CREATE TABLE IF NOT EXISTS results_average (L integer, mask_A integer, mask_C integer, lambda real, V real, mu real, beta real, t real, I3 real, I3_std real, I3_2 real, I3_2_std real, I3_min real, I3_min_std real, runtime real, zero_magnetization_ipr real, zero_magnetization_ipr_std real,  zero_magnetization_participation_Shannon_entropy real, zero_magnetization_participation_Shannon_entropy_std real, zero_magnetization_participation_collision_entropy real, zero_magnetization_participation_collision_entropy_std real, zero_magnetization_participation_min_entropy real, zero_magnetization_participation_min_entropy_std real, consecutive_level_spacing_ratio real, consecutive_level_spacing_ratio_std real, samplesize integer, CONSTRAINT results_average_uniq UNIQUE (L, mask_A, mask_C, lambda, V, mu, beta, t));'
echo "PRAGMA temp_store_directory = '/scratch/niklas.boelter/tmp';"
echo "PRAGMA temp_store = 2;"
echo 'REPLACE INTO results_average SELECT L, mask_A, mask_C, lambda, V, mu, beta, t, avg(I3), stdev(I3), avg(I3_2), stdev(I3_2), avg(I3_min), stdev(I3_min), avg(runtime), avg(zero_magnetization_ipr), stdev(zero_magnetization_ipr), avg(zero_magnetization_participation_Shannon_entropy), stdev(zero_magnetization_participation_Shannon_entropy), avg(zero_magnetization_participation_collision_entropy), stdev(zero_magnetization_participation_collision_entropy), avg(zero_magnetization_participation_min_entropy), stdev(zero_magnetization_participation_min_entropy), avg(consecutive_level_spacing_ratio), stdev(consecutive_level_spacing_ratio), COUNT(*) FROM results GROUP BY L, mask_A, mask_C, lambda, V, mu, beta, t;'
echo 'VACUUM;'
echo "ATTACH DATABASE '$HOME/qubit-chain-tfd/xxz-disorder-random-states-$boundary_conditions.db' AS dst;"
echo 'CREATE TABLE IF NOT EXISTS dst.results_average (L integer, mask_A integer, mask_C integer, lambda real, V real, mu real, beta real, t real, I3 real, I3_std real, I3_2 real, I3_2_std real, I3_min real, I3_min_std real, runtime real, zero_magnetization_ipr real, zero_magnetization_ipr_std real,  zero_magnetization_participation_Shannon_entropy real, zero_magnetization_participation_Shannon_entropy_std real, zero_magnetization_participation_collision_entropy real, zero_magnetization_participation_collision_entropy_std real, zero_magnetization_participation_min_entropy real, zero_magnetization_participation_min_entropy_std real, consecutive_level_spacing_ratio real, consecutive_level_spacing_ratio_std real, samplesize integer, CONSTRAINT results_average_uniq UNIQUE (L, mask_A, mask_C, lambda, V, mu, beta, t));'
echo "REPLACE INTO dst.results_average SELECT * FROM results_average WHERE L = $number_of_spins;"
echo 'VACUUM dst;'
