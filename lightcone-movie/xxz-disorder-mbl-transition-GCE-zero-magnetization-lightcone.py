#!/usr/bin/env python3
# -*- coding: utf8 -*-
from matplotlib.animation import FuncAnimation, FFMpegWriter
from os.path import expanduser
import colorcet
import copy
import cycler
import matplotlib
import matplotlib.cm
import matplotlib.colors
import matplotlib.pyplot as pyplot
import numpy
import scipy.special
import sqlite3

home = expanduser("~")

pyplot.rcParams['axes.prop_cycle'] = cycler.cycler('color', ["#d70000", "#8c3cff", "#028800", "#00acc7", "#e7a500", "#ff7fd1", "#6c004f", "#1fff00", "#00009d", "#867068", "#004942", "#4f2a00"])

pyplot.rcParams['text.usetex'] = True
pyplot.rcParams['text.latex.preamble'] = r"\usepackage{amsmath}"
pyplot.rcParams['figure.autolayout'] = True
home = expanduser("~")
con = sqlite3.connect(":memory:")
con.enable_load_extension(True)
con.load_extension("../cksumvfs")

con = sqlite3.connect(home + "/xxz-chain-with-disorder-data/rocks/xxz-disorder-random-states-OBC.db")
con.enable_load_extension(True)
con.execute("select load_extension('../libsqlitefunctions')")
con.enable_load_extension(False)
c = con.cursor()

N = 12
for mask_A in (3,):#, 1):

    if mask_A == 1:
        mask_C_str = '(' + ', '.join([str(2**i) for i in range(1, N)]) + ')'
        grid_size = N - 1
        max_t = 100.1
    if mask_A == 3:
        mask_C_str = '(' + ', '.join([str(2**i + 2**(i + 1)) for i in range(2, N - 1)]) + ')'
        grid_size = N - 3
        max_t = 500.1

    beta = scipy.special.binom(N, N // 2)

    framenumber = 1

    frame_parameters = []

    mode = 'time'
    min_samplesize = float("inf")
    for renyi in ['I3', 'I3_2']:
        # sqlite> SELECT L, AVG(S_AC+S_AD), AVG(S2_AC+S2_AD), COUNT(*) FROM results_AC_AD_Haar WHERE popcount(mask_A) = 1 AND popcount(mask_C) = 1 GROUP BY L;
        # 12|12.4090488718291|12.2667830341439|6001
        if mask_A == 1:
            if renyi == 'I3':
                I3_Haar = -0.5573
            elif renyi == 'I3_2':
                I3_Haar = -0.415034

        # sqlite> SELECT L, AVG(S_AC+S_AD), AVG(S2_AC+S2_AD), COUNT(*) FROM results_AC_AD_Haar WHERE popcount(mask_A) = 2 AND popcount(mask_C) = 2 GROUP BY L;
        # 12|14.6122647099474|14.3107794385657|5500
        if mask_A == 3:
            if renyi == 'I3':
                I3_Haar = -1.86336
            elif renyi == 'I3_2':
                I3_Haar = -1.73884

        for mode in 'time', 'disorder':
            # for mode in ['disorder']:
            for Delta in numpy.linspace(-4, 4, 17):

                # if Delta != 1.0:
                #     continue

                if mode == 'time':
                    distinct_param = numpy.array(list(zip(
                        *(c.execute("SELECT DISTINCT t FROM results_average WHERE L = ? AND mask_A = ? AND mask_C IN " + mask_C_str + " AND V in (0.0, 0.5, 1.0, 2.0, 4.0, 6.0, 8.0, 10.0, 100.0) AND beta = ? AND mu = 0.0 AND lambda = ? AND t < ? ORDER BY t ASC", (N, mask_A, beta, Delta, max_t))))))
                elif mode == 'disorder':
                    distinct_param = numpy.array(list(zip(
                        *(c.execute("SELECT DISTINCT V FROM results_average WHERE L = ? AND mask_A = ? AND mask_C IN " + mask_C_str + " AND V in (0.0, 0.5, 1.0, 2.0, 4.0, 6.0, 8.0, 10.0, 100.0) AND beta = ? AND mu = 0.0 AND lambda = ? ORDER BY V ASC", (N, mask_A, beta, Delta))))))

                if len(distinct_param) == 0:
                    continue

                distinct_param = distinct_param[0]

                # for t in distinct_param:
                #     data = numpy.array(list(zip(
                #         *(c.execute("SELECT mask_C, V as W, I3_2, I3_2_std, samplesize FROM results_average WHERE L = ? AND mask_A = 1 AND mask_C IN (1,2,4,8,16,32,64,128,256,512,1024,2048,4096, 8192) AND V in (0.0, 0.5, 1.0, 2.0, 4.0, 6.0, 8.0, 10.0, 100.0) AND beta = ? AND mu = 0.0 AND lambda = ? AND t = ? ORDER BY V DESC, mask_C ASC", (N, beta, Delta, t))))))

                #     if len(data) == 0:
                #         continue

                #     data[0] = numpy.log2(data[0])
                #     grid = data[2].reshape((len(data[2])//12, 12))
                #     pyplot.figure()
                #     pyplot.title(rf"$\Delta = {Delta}, t = {t}$")
                #     pyplot.imshow(grid)
                #     pyplot.show()

                fig, ax = pyplot.subplots(figsize=(8 / 2.54, 6 / 2.54))

                # pyplot.xlabel(rf"Qubit Index $i$")
                pyplot.xlabel(rf"Subsystem separation $d$")
                if mode == 'time':
                    pyplot.ylabel(rf"Disorder Strength $W$")
                    grid = numpy.zeros((9, grid_size))
                    pyplot.yticks([0, 1, 2, 3, 4, 5, 6, 7, 8], ['100', '10', '8', '6', '4', '2', '1', '0.5', '0.0'])
                    pyplot.xticks([0, 2, 4, 6, 8, 10])
                if mode == 'disorder':
                    pyplot.ylabel(rf"Time $t$")
                    print("len(distinct_param) = ", len(distinct_param))

                    distinct_t = numpy.array(list(zip(
                        *(c.execute("SELECT DISTINCT t  FROM results_average WHERE L = ? AND mask_A = ? AND mask_C IN " + mask_C_str + " AND V in (0.0, 0.5, 1.0, 2.0, 4.0, 6.0, 8.0, 10.0, 100.0) AND beta = ? AND mu = 0.0 AND lambda = ? AND V = ? AND t < ? ORDER BY t ASC, mask_C ASC", (N, mask_A, beta, Delta, distinct_param[0], max_t))))))[0]

                    print(distinct_t, len(distinct_t))

                    grid = numpy.zeros((len(distinct_t), 12))
                    tick_locations = []
                    tick_labels = []

                    for i in [0, 100, 200, 290, 380]:
                        if i < len(distinct_t):
                            tick_locations.append(i)
                            tick_labels.append(int(distinct_t[i]))

                    pyplot.yticks(tick_locations, tick_labels)

                limits = list(c.execute(f"SELECT min({renyi}/{I3_Haar}), max({renyi}/{I3_Haar}) FROM results_average WHERE L = ? AND mask_A = ? AND mask_C IN " + mask_C_str +
                                        " AND V in (0.0, 0.5, 1.0, 2.0, 4.0, 6.0, 8.0, 10.0, 100.0) AND beta = ? AND mu = 0.0 AND lambda = ? AND t < ? AND samplesize > 0;", (N, mask_A, beta, Delta, max_t)))

                print(limits)
                if renyi == 'I3':
                    limits[0] = (limits[0][0], 1.0)
                print(limits)

                # norm = matplotlib.colors.Normalize(vmin=limits[0][0], vmax=limits[0][1], clip=True)
                norm = matplotlib.colors.Normalize(vmin=limits[0][0], vmax=limits[0][1])
                print(limits[0][0], limits[0][1])
                # if limits[0][0] < -0.05 or limits[0][1] > 1.05:
                #     print("Adjust limits!!")
                #     exit()

                cmap = copy.copy(colorcet.cm.fire)
                cmap.set_under("blue")
                cmap.set_over("green")

                if mode == 'time':
                    im = pyplot.imshow(grid, norm=norm, cmap=cmap)
                elif mode == 'disorder':
                    im = pyplot.imshow(grid, norm=norm, cmap=cmap, aspect='auto', origin='lower')

                mapper = matplotlib.cm.ScalarMappable(norm=norm, cmap=cmap)
                if renyi == 'I3':
                    cb = pyplot.colorbar(mappable=mapper, label=r"Rescaled Tripartite Information $\tilde I_3$")
                elif renyi == 'I3_2':
                    cb = pyplot.colorbar(mappable=mapper, label=r"Rescaled Tripartite Information $\tilde I_3^{(2)}$")
                elif renyi == 'I3_min':
                    cb = pyplot.colorbar(mappable=mapper, label=r"Rescaled Tripartite Information $\tilde I_3^{\mathrm{min}}$")

                def init():
                    global grid

                    t = 0.0

                    print(N, beta, Delta, t)

                    if mode == 'time':
                        data = numpy.array(list(zip(
                            *(c.execute(f"SELECT mask_C, V, {renyi}/{I3_Haar}, {renyi}_std/{I3_Haar}, samplesize FROM results_average WHERE L = ? AND mask_A = ? AND mask_C IN " + mask_C_str + " AND V in (0.0, 0.5, 1.0, 2.0, 4.0, 6.0, 8.0, 10.0, 100.0) AND beta = ? AND mu = 0.0 AND lambda = ? AND t = ? ORDER BY V DESC, mask_C ASC", (N, mask_A, beta, Delta, t))))))
                        grid = data[2].reshape((len(data[2]) // grid_size, grid_size))
                    if mode == 'disorder':
                        data = numpy.array(list(zip(
                            *(c.execute(f"SELECT mask_C, t, {renyi}/{I3_Haar}, {renyi}_std/{I3_Haar}, samplesize FROM results_average WHERE L = ? AND mask_A = ? AND mask_C IN " + mask_C_str + " AND V in (0.0, 0.5, 1.0, 2.0, 4.0, 6.0, 8.0, 10.0, 100.0) AND beta = ? AND mu = 0.0 AND lambda = ? AND V = ? AND t < 50.1 ORDER BY t ASC, mask_C ASC", (N, mask_A, beta, Delta, t))))))
                        if len(data) == 0 or len(data[2]) % grid_size != 0:
                            print("Skipping init", data)
                            return []
                        grid = data[2].reshape((len(data[2]) // grid_size, grid_size))
                        # print("len(distinct_t) = ", len(data[2]) / 12)

                    # mapper = matplotlib.cm.ScalarMappable(norm=norm, cmap=colorcet.cm.CET_R2)
                    # cb = pyplot.colorbar(mappable=mapper, label="Rescaled Tripartite Information $\tilde I_3$")

                    im.set_data(grid)
                    # im.set_norm(norm)
                    # im.set_cmap('cet_fire')
                    if mode == 'time':
                        # ax.set_title(rf"XXZ Chain with Disorder, $N = {N}, \Delta = {Delta}, n = {int(beta)}, t = \phantom{{00}}0$")
                        pass
                    if mode == 'disorder':
                        # ax.set_title(rf"XXZ Chain with Disorder, $N = {N}, \Delta = {Delta}, n = {int(beta)}, W = 0.0$")
                        pass
                    return im,

                def update(frame):
                    global min_samplesize
                    t = distinct_param[int(frame)]
                    # print(t)
                    if mode == 'time':
                        data = numpy.array(list(zip(
                            *(c.execute(f"SELECT mask_C, V, avg({renyi})/{I3_Haar}, avg({renyi}_std)/{I3_Haar}, sum(samplesize) FROM results_average WHERE L = ? AND mask_A = ? AND mask_C IN " + mask_C_str + " AND V in (0.0, 0.5, 1.0, 2.0, 4.0, 6.0, 8.0, 10.0, 100.0) AND beta = ? AND mu = 0.0 AND lambda = ? AND t - 0.1 < ? AND t + 0.1 > ? GROUP BY mask_C, V ORDER BY V DESC, mask_C ASC", (N, mask_A, beta, Delta, t, t))))))

                    if mode == 'disorder':
                        data = numpy.array(list(zip(
                            *(c.execute(f"SELECT mask_C, t, {renyi}/{I3_Haar}, {renyi}_std/{I3_Haar}, samplesize FROM results_average WHERE L = ? AND mask_A = ? AND mask_C IN " + mask_C_str + " AND V in (0.0, 0.5, 1.0, 2.0, 4.0, 6.0, 8.0, 10.0, 100.0) AND beta = ? AND mu = 0.0 AND lambda = ? AND V = ? AND t < 50.1 ORDER BY t ASC, mask_C ASC", (N, mask_A, beta, Delta, t))))))
                    if len(data[2]) % grid_size != 0:
                        print("Skipping param", t)
                        return []
                    grid = data[2].reshape((len(data[2]) // grid_size, grid_size))
                    
                    samplesize = min(data[4])

                    if samplesize < 90:
                        print(f"t = {t}")
                        print(f"samplesize = {samplesize}")
                        print(f"V = {data[1][numpy.argmin(data[4])]}")

                    if(samplesize < min_samplesize):
                        min_samplesize = samplesize

                    # norm = matplotlib.colors.Normalize(vmin=min(data[2]), vmax=max(data[2]), clip=True)
                    # mapper = matplotlib.cm.ScalarMappable(norm=norm, cmap=colorcet.cm.CET_R2)
                    # cb = pyplot.colorbar(mappable=mapper, label="Rescaled Tripartite Information $\tilde I_3$")

                    im.set_data(grid)
                    # im.set_norm(norm)
                    # im.set_cmap('cet_fire')
                    if mode == 'time':
                        t_str = str(int(t))
                        t_str = (r'\phantom{0}' * (3 - len(t_str))) + t_str
                        ax.set_title(rf"$t = {t_str}$")

                        # ax.set_title(rf"XXZ Chain with Disorder, $N = {N}, \Delta = {Delta}, n = {int(beta)}, t = {t_str}$")
                    else:
                        t_str = str(t)
                        print(t_str)
                        # ax.set_title(rf"XXZ Chain with Disorder, $N = {N}, \Delta = {Delta}, n = {int(beta)}, W = {t_str}$")
                    return im,

                #         # if W == 0:
                #         #     my_plots.append(pyplot.plot(data[0], -1 * data[1], label=rf"$d = {int(numpy.log2(mask_C)-1)}$")[0])
                #         # else:
                #         # ln = pyplot.plot(data[0], -1 * data[1], label=rf"$d = {int(numpy.log2(mask_C))}$")
                #         # my_plots.append(ln[0])
                #         ln = pyplot.errorbar(data[0], -1 * data[1], data[2]/numpy.sqrt(data[3]), label=rf"$d = {int(numpy.log2(mask_C))}$", ecolor="#666666", capsize=2)

                #         for ln2 in ln[1]:
                #             my_plots.append(ln2)

                # #

                #     pyplot.title(rf"XXZ Chain with $\Delta = {Delta}, W = {W}, n = {int(beta)}$")
                #     pyplot.ylim([-0.01, 1.0])
                #     # pyplot.xlim([0, 20])
                #     pyplot.legend(ncol=2)
                #     pyplot.ylabel(r"Average Negative Rescaled Tripartite Information $\tilde \langle -I_3(t) \rangle_\mathrm{dis}$")
                #     pyplot.xlabel(r"Time $t$")
                #     # pyplot.show()

                #     # print(my_plots)

                #     return my_plots

                frames = numpy.linspace(0, len(distinct_param) - 1, len(distinct_param))

                if mode == 'time':
                    FFwriter = FFMpegWriter(fps=15)
                    ani = FuncAnimation(fig, update, frames=frames, init_func=init, blit=False)
                    ani.save(f'xxz-disorder-{renyi}-N{N}-a{mask_A}-OBC-n{int(beta)}-Delta{Delta}-butterfly.mp4', writer=FFwriter, dpi=300)

                if mode == 'disorder':
                    for frame in frames:
                        if len(init()) == 0:
                            continue
                        if len(update(frame)) == 0:
                            continue
                        V = distinct_param[int(frame)]
                        filename = f'xxz-disorder-{renyi}-N{N}-a{mask_A}-OBC-n{int(beta)}-Delta{Delta}-butterfly-W{V}.png'
                        pyplot.savefig(filename, format='png', dpi=300)
                        print("Written", filename)

                if mode == 'time':
                    for frame in range(0, len(distinct_param), 20):
                        init()
                        if len(update(frame)) == 0:
                            continue
                        filename = f'frames/xxz-disorder-{renyi}-N{N}-a{mask_A}-OBC-n{int(beta)}-Delta{Delta}-butterfly-{str(distinct_param[int(frame)])}.png'
                        pyplot.savefig(filename, format='png', dpi=300, transparent=True)
                        print("Written", filename)
    print(f"min_samplesize = {min_samplesize}")
