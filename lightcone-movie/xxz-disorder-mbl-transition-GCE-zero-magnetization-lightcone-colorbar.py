#!/usr/bin/env python3
# -*- coding: utf8 -*-
import sqlite3
from os.path import expanduser
import matplotlib.pyplot as pyplot
import matplotlib
import matplotlib.cm
import matplotlib.colors
import numpy
import itertools
import colorcet
from colorcet.plotting import swatch, swatches
import shutil
import cycler
from matplotlib.animation import FuncAnimation, FFMpegWriter
import scipy.special

home = expanduser("~")

pyplot.rcParams['figure.autolayout'] = True
con = sqlite3.connect(":memory:")
con.enable_load_extension(True)
con.load_extension("../cksumvfs")

con = sqlite3.connect(home + "/rocks/xxz-disorder-random-states-OBC.db")
con.enable_load_extension(True)
con.execute("select load_extension('../libsqlitefunctions')")
con.enable_load_extension(False)
c = con.cursor()

import holoviews as hv
hv.extension('bokeh')

for mask_A in (1,3):

    Delta = 1.0
    N = 12

    if mask_A == 1:
        mask_C_str = '(' + ', '.join([str(2**i) for i in range(1, N)]) + ')'
        grid_size = N - 1
    if mask_A == 3:
        mask_C_str = '(' + ', '.join([str(2**i + 2**(i+1)) for i in range(2, N-1)]) + ')'
        grid_size = N - 3

    beta = scipy.special.binom(N, N//2)

    for renyi in ['I3', 'I3_2']:
        fig = pyplot.figure(figsize=(13/2.54, 2.5/2.54))
        ax = pyplot.gca()
        # sqlite> SELECT L, AVG(S_AC+S_AD), AVG(S2_AC+S2_AD), COUNT(*) FROM results_AC_AD_Haar WHERE popcount(mask_A) = 1 AND popcount(mask_C) = 1 GROUP BY L;
        # 12|12.4090488718291|12.2667830341439|6001
        if mask_A == 1:
            if renyi == 'I3':
                I3_Haar = -0.5573
            elif renyi == 'I3_2':
                I3_Haar = -0.415034

        # sqlite> SELECT L, AVG(S_AC+S_AD), AVG(S2_AC+S2_AD), COUNT(*) FROM results_AC_AD_Haar WHERE popcount(mask_A) = 2 AND popcount(mask_C) = 2 GROUP BY L;
        # 12|14.6122647099474|14.3107794385657|5500
        if mask_A == 3:
            if renyi == 'I3':
                I3_Haar = -1.86336
            elif renyi == 'I3_2':
                I3_Haar = -1.73884

        limits = list(c.execute(f"SELECT min({renyi}/{I3_Haar}), max({renyi}/{I3_Haar}) FROM results_average WHERE L = ? AND mask_A = ? AND mask_C IN " + mask_C_str + " AND V in (0.0, 0.5, 1.0, 2.0, 4.0, 6.0, 8.0, 10.0, 100.0) AND beta = ? AND mu = 0.0 AND lambda = ? AND t < 500.1 AND samplesize > 20;", (N, mask_A, beta, Delta)))
        if renyi == 'I3':
            limits[0] = (limits[0][0], 1.0)

        norm = matplotlib.colors.Normalize(vmin=limits[0][0], vmax=limits[0][1]) 
        mapper = matplotlib.cm.ScalarMappable(norm=norm, cmap=colorcet.cm.fire)
        cb = pyplot.colorbar(mappable=mapper, label=r"Rescaled Tripartite Information $\tilde I_3$", orientation='horizontal', cax=ax)

        # pyplot.show()

        filename = f'xxz-disorder-mbl-transition-Delta{Delta}-a{mask_A}-{renyi}-lightcone-colorbar.png'
        pyplot.savefig(filename, format='png', dpi=300, transparent=True)
        print("Written", filename)
