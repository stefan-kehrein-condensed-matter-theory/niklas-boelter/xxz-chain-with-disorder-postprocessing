import numpy


def most_significant_bit(x):
    return (2**(numpy.log2(x)).astype(int)).astype(int)


def least_significant_bit(x):
    msb = most_significant_bit(x)
    while (x > msb).any():
        x[x > msb] = x[x > msb] - msb[x > msb]
        msb = most_significant_bit(x)
    return x


def distance_subsystems(a, b):
    a = numpy.array(a)
    b = numpy.array(b)
    return (numpy.log2(least_significant_bit(a)) - numpy.log2(most_significant_bit(b)) - 1).astype(int)
