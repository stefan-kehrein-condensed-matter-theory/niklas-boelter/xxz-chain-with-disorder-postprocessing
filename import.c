#include <sqlite3.h>
#include <stdlib.h>
#include <stdio.h>
#include <sysexits.h>

static int callback(void *data, int argc, char **argv, char **azColName){
   int i;
   fprintf(stderr, "%s: ", (const char*)data);
   
   for(i = 0; i<argc; i++){
      printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
   }
   
   // printf("\n");
   return 0;
}

int main(int argc, char *argv[])
{
	sqlite3 *db;
	int rc;
	char *zErrMsg = 0;
	sqlite3_stmt *stmt;
	char query[10000];

	if(argc != 5) {
	    printf("Usage: %s source.db source_tablename target.db target_tablename\n\n", argv[0]);
	    exit(EX_USAGE);
	}


	rc = sqlite3_open(":memory:", &db);
	if(rc) {
	    fprintf(stderr, "\n001: Can't open database: %s\n", sqlite3_errmsg(db));
	    exit(EX_SOFTWARE);
	}

	rc = sqlite3_db_config(db, SQLITE_DBCONFIG_ENABLE_LOAD_EXTENSION, 1, NULL);
	if(rc) {
		fprintf(stderr, "\n002: Cannot enable load_extension(): %s\n", sqlite3_errmsg(db));
	    exit(EX_SOFTWARE);
    }

	rc = sqlite3_load_extension(db, "/home/niklas.boelter/xxz-chain-with-disorder-postprocessing/cksumvfs.so", 0, &zErrMsg);
	if(rc) {
		fprintf(stderr, "\n003: Cannot load extension cksumvfs: %s\n", zErrMsg);
        exit(EX_SOFTWARE);
	}
	
    rc = sqlite3_load_extension(db, "/home/niklas.boelter/xxz-chain-with-disorder-postprocessing/libsqlitefunctions.so", 0, &zErrMsg);
	if(rc) {
		fprintf(stderr, "\n003: Cannot load extension libsqlitefunctions: %s\n", zErrMsg);
	    exit(EX_SOFTWARE);
    }

	rc = sqlite3_close(db);
	if(rc) {
	    fprintf(stderr, "\n004: Can't close database: %s\n", sqlite3_errmsg(db));
	    exit(EX_SOFTWARE);
	}

	rc = sqlite3_open(argv[1], &db);
	if(rc) {
	    fprintf(stderr, "\n005: Can't open database: %s\n", sqlite3_errmsg(db));
	    exit(EX_SOFTWARE);
	}

	snprintf(query, sizeof(query), "ATTACH DATABASE '%s' AS target;", argv[3]);
	rc = sqlite3_exec(db, query, NULL, 0, &zErrMsg);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\n006: SQL error %i: %s (%s)\n", rc, zErrMsg, query);
        sqlite3_free(zErrMsg);
        exit(EX_SOFTWARE);
    }
/*
    snprintf(query, sizeof(query), "SELECT count(*) FROM target.%s;", argv[4]);
	rc = sqlite3_exec(db, query, callback, "Count before import", &zErrMsg);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\n007: SQL error %i: %s (%s)\n", rc, zErrMsg, query);
        sqlite3_free(zErrMsg);
        exit(EX_SOFTWARE);
    }
*/
    snprintf(query, sizeof(query), "INSERT INTO target.%s SELECT * FROM main.%s WHERE 1 ON CONFLICT DO NOTHING;", argv[4], argv[2]);
	rc = sqlite3_exec(db, query, NULL, 0, &zErrMsg);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\n008: SQL error %i: %s (%s)\n", rc, zErrMsg, query);
        sqlite3_free(zErrMsg);
        exit(EX_SOFTWARE);
    }
/*
    snprintf(query, sizeof(query), "SELECT count(*) FROM target.%s;", argv[4]);
	rc = sqlite3_exec(db, query, callback, " Count after import", &zErrMsg);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\n009: SQL error %i: %s (%s)\n", rc, zErrMsg, query);
        sqlite3_free(zErrMsg);
        exit(EX_SOFTWARE);
    }
*/
    rc = sqlite3_close(db);
	if(rc) {
	    fprintf(stderr, "\n010: Can't close database: %s\n", sqlite3_errmsg(db));
	    exit(EX_SOFTWARE);
	}
}
