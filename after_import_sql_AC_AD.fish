#!/usr/bin/env fish
if test (count $argv) -ne 2;
    echo "Usage:" (status filename) "number_of_spins boundary_conditions" 1>&2;
    exit -1
end
set number_of_spins $argv[1]
set boundary_conditions $argv[2]

echo ".load $HOME/qubit-chain-tfd/cksumvfs.so"
echo ".open /scratch/niklas.boelter/rocks-rawdata/xxz-disorder-random-states-$boundary_conditions-N$number_of_spins.db"
echo ".load $HOME/qubit-chain-tfd/popcount.so"
echo ".load $HOME/qubit-chain-tfd/libsqlitefunctions.so"
echo 'CREATE TABLE IF NOT EXISTS results_average (L integer, mask_A integer, mask_C integer, lambda real, V real, mu real, beta real, t real, I3 real, I3_std real, I3_2 real, I3_2_std real, I3_min real, I3_min_std real, runtime real, zero_magnetization_ipr real, zero_magnetization_ipr_std real,  zero_magnetization_participation_Shannon_entropy real, zero_magnetization_participation_Shannon_entropy_std real, zero_magnetization_participation_collision_entropy real, zero_magnetization_participation_collision_entropy_std real, zero_magnetization_participation_min_entropy real, zero_magnetization_participation_min_entropy_std real, consecutive_level_spacing_ratio real, consecutive_level_spacing_ratio_std real, samplesize integer, CONSTRAINT results_average_uniq UNIQUE (L, mask_A, mask_C, lambda, V, mu, beta, t));'
echo "PRAGMA temp_store_directory = '/scratch/niklas.boelter/tmp';"
echo "PRAGMA temp_store = 2;"
echo 'REPLACE INTO results_average SELECT r.L, r.mask_A, r.mask_C, r.lambda, r.V, r.mu, r.beta, r.t, A.S + B.S + C.S + D.S - avg(AB.S + r.S_AC + r.S_AD), stdev(r.S_AC + r.S_AD), A.S2 + B.S2 + C.S2 + D.S2 - avg(AB.S2 + r.S2_AC + r.S2_AD), stdev(r.S2_AC + r.S2_AD), A.Smin + B.Smin + C.Smin + D.Smin - avg(AB.Smin + r.Smin_AC + r.Smin_AD), stdev(r.Smin_AC + r.Smin_AD), avg(r.runtime), avg(r.zero_magnetization_ipr), stdev(r.zero_magnetization_ipr), avg(r.zero_magnetization_participation_Shannon_entropy), stdev(r.zero_magnetization_participation_Shannon_entropy), avg(r.zero_magnetization_participation_collision_entropy), stdev(r.zero_magnetization_participation_collision_entropy), avg(r.zero_magnetization_participation_min_entropy), stdev(r.zero_magnetization_participation_min_entropy), avg(r.consecutive_level_spacing_ratio), stdev(r.consecutive_level_spacing_ratio), COUNT(*) FROM results_AC_AD AS r, subsystem_entropies AS A, subsystem_entropies AS B, subsystem_entropies AS C, subsystem_entropies AS D, subsystem_entropies AS AB WHERE A.N = r.L AND A.l = popcount(r.mask_A) AND B.N = r.L AND B.l = (r.L - popcount(r.mask_A)) AND C.N = r.L AND C.l = popcount(r.mask_C) AND D.N = r.L AND D.l = (r.L - popcount(r.mask_C)) AND AB.N = r.L AND AB.l = r.L GROUP BY r.L, r.mask_A, r.mask_C, r.lambda, r.V, r.mu, r.beta, r.t;'
echo 'VACUUM;'
echo "ATTACH DATABASE '$HOME/qubit-chain-tfd/xxz-disorder-random-states-$boundary_conditions.db' AS dst;"
echo 'CREATE TABLE IF NOT EXISTS dst.results_average (L integer, mask_A integer, mask_C integer, lambda real, V real, mu real, beta real, t real, I3 real, I3_std real, I3_2 real, I3_2_std real, I3_min real, I3_min_std real, runtime real, zero_magnetization_ipr real, zero_magnetization_ipr_std real,  zero_magnetization_participation_Shannon_entropy real, zero_magnetization_participation_Shannon_entropy_std real, zero_magnetization_participation_collision_entropy real, zero_magnetization_participation_collision_entropy_std real, zero_magnetization_participation_min_entropy real, zero_magnetization_participation_min_entropy_std real, consecutive_level_spacing_ratio real, consecutive_level_spacing_ratio_std real, samplesize integer, CONSTRAINT results_average_uniq UNIQUE (L, mask_A, mask_C, lambda, V, mu, beta, t));'
echo "REPLACE INTO dst.results_average SELECT * FROM results_average WHERE L = $number_of_spins;"
echo 'VACUUM dst;'
