#!/usr/bin/env python3
# -*- coding: utf8 -*-
from os.path import expanduser
import colorcet
import itertools
import matplotlib.cm
import matplotlib.colors
import matplotlib.pyplot as pyplot
import numpy
import scipy.special
import sqlite3

colorcycle = pyplot.rcParams['axes.prop_cycle'].by_key()['color']
pyplot.rcParams['text.usetex'] = True
pyplot.rcParams['text.latex.preamble'] = r"\usepackage{amsmath}"
pyplot.rcParams['figure.autolayout'] = True
home = expanduser("~")

norm = matplotlib.colors.Normalize(vmin=2, vmax=10)
mapper = matplotlib.cm.ScalarMappable(norm=norm, cmap=colorcet.cm.CET_R2)

for renyi in ["I3", "I3_2"]:
    L = 12

    columnnames = {
        'level-spacing-ratio': 'consecutive_level_spacing_ratio',
        'inverse-participation-ratio': 'zero_magnetization_ipr',
        'participation-entropy': 'zero_magnetization_participation_Shannon_entropy'
     }

    for comparison in ['level-spacing-ratio', 'inverse-participation-ratio', 'participation-entropy']:
        for Delta in [0.0, 1.0]:
            con = sqlite3.connect(":memory:")
            con.enable_load_extension(True)
            con.load_extension("../cksumvfs")

            con = sqlite3.connect(home + "/xxz-chain-with-disorder-data/loki/xxz-disorder-random-states-OBC.db")

            con.enable_load_extension(True)
            con.load_extension("../libsqlitefunctions")
            con.enable_load_extension(False)
            c = con.cursor()

            for mask_A in (1, 3):
                beta = scipy.special.binom(L, L // 2)
                if mask_A == 1:
                    mask_C = 2**(L - 1)
                elif mask_A == 3:
                    mask_C = 2**(L - 1) + 2**(L - 2)
                else:
                    raise Exception(f"Unknown mask_A = {mask_A}")
                if mask_A == 1:
                    if renyi == 'I3':
                        I3_Haar = -0.5573
                    elif renyi == 'I3_2':
                        I3_Haar = -0.415034

                # sqlite> SELECT L, AVG(S_AC+S_AD), AVG(S2_AC+S2_AD), COUNT(*) FROM results_AC_AD_Haar WHERE popcount(mask_A) = 2 AND popcount(mask_C) = 2 GROUP BY L;
                # 12|14.6122647099474|14.3107794385657|5500
                if mask_A == 3:
                    if renyi == 'I3':
                        I3_Haar = -1.86336
                    elif renyi == 'I3_2':
                        I3_Haar = -1.73884

                print(f"L = {L}, mask_A = {mask_A}, mask_C = {mask_C}")

                fig, ax1 = pyplot.subplots(figsize=(4, 3))
                if Delta == 0.0:
                    ax2 = fig.add_axes([0.62, 0.6, 0.3, 0.3])
                if Delta == 1.0:
                    ax2 = fig.add_axes([0.52, 0.5, 0.4, 0.4])
                ax1.set_xscale('log')
                ax2.set_xscale('log')
                ax1.set_xlim([0.4, 110])
                ax1.set_ylim([-0.05, 1.05])
                ax2.set_xlim([0.4, 110])

                for t in [2**n for n in range(2, 11)]:
                    data = numpy.array(list(zip(
                        *(c.execute(f"SELECT a2.V, (a2.{renyi} - a1.{renyi})/({I3_Haar} - a1.{renyi}), a2.{renyi}_std/(ABS({I3_Haar} - a1.{renyi})*sqrt(a2.samplesize)) FROM results_average a1, results_average a2 WHERE a1.L = ? AND a1.L = a2.L AND a1.mask_A = ? AND a2.mask_A = a1.mask_A AND a1.mask_C = ? AND a2.mask_C = a1.mask_C AND a1.beta = ? AND a2.beta = a1.beta AND a1.mu = 0.0 AND a2.mu = a1.mu AND a1.lambda = ? AND a2.lambda = a1.lambda AND a2.samplesize > 99 AND a1.t = 0.0 AND a2.t = ? AND a1.V > 0 AND a2.V = a1.V", (L, mask_A, mask_C, beta, Delta, t))))))
                    print(data)
                    if len(data) != 3:
                        print(f"Skipping t = {t}")
                        continue
                    print(data[0])
                    
                    ax1.errorbar(data[0], data[1], data[2], fmt='x-', markersize=5, capsize=1.5, label=rf"$t = {t}$", color=mapper.to_rgba(numpy.log2(t)))
                    # ax2.errorbar([float('nan')],[float('nan')], [float('nan')], fmt='.', markersize=5,  capsize=1.5, label=rf"$I_3/I_3^\textrm{{Haar}}, t = {t}$")
                    # ax2.errorbar(data[0], data[3], data[4], fmt='.', markersize=5, capsize=1.5, label=r"Consecutive Level Spacing Ratio $\langle r \rangle$")
                data = numpy.array(list(zip(
                    *(c.execute(f"SELECT V, AVG({columnnames[comparison]}), AVG({columnnames[comparison]}_std/(sqrt(samplesize))) FROM results_average WHERE L = ? AND mask_A = ? AND mask_C = ? AND beta = ? AND mu = 0.0 AND lambda = ? AND samplesize > 99 AND V > 0 GROUP BY V", (L, mask_A, mask_C, beta, Delta))))))
                print(len(data), data.shape)
                if len(data) != 3:
                    continue
                
                
                ax2.errorbar(data[0], data[1], data[2], fmt='o-', markersize=4, capsize=1.5, label=r"ED", color='black')
                
                ax1.set_xlabel('Disorder Strength $W$')
                ax2.set_xlabel('Disorder Strength $W$')

                ax1.set_ylabel(r"Rescaled Tripartite Information $\tilde I_3$")
                if comparison == 'level-spacing-ratio':
                    ax2.set_ylabel(r"$\langle r \rangle$")
                    r_GOE = 0.5307
                    r_Poisson = 0.386
                    ax2.set_ylim([r_Poisson - (r_GOE - r_Poisson) * 0.05, r_GOE + (r_GOE - r_Poisson) * 0.05])
                    ax2.plot([-10, 120], [r_GOE, r_GOE], '-', color='tab:blue', label=r"GOE")
                    ax2.plot([-10, 120], [r_Poisson, r_Poisson], '--', color='tab:orange', label=r"Poisson")
                    ax2.legend(loc='center right')
                if comparison == 'inverse-participation-ratio':
                    ax2.set_ylabel(r"IPR")
                    ax2.legend()
                if comparison == 'participation-entropy':
                    ax2.set_ylabel(r"$S\left\{|\langle \Psi|i\rangle|^2\right\}$")
                    ax2.legend()

                # pyplot.title(rf"XXZ Chain with $L = {L}, \Delta = {Delta}, n = {int(beta)}, A = [1], C = [{L}]$")

                # handles,labels = ax2.get_legend_handles_labels()

                # handles = [handles[2], handles[3], handles[0], handles[1]]
                # labels = [labels[2], labels[3], labels[0], labels[1]]

                # ax2.legend(handles,labels)
                
                if Delta == 0.0:
                    ax1.legend(loc=(0.14, 0.27), ncol=1, fontsize='small')
                # pyplot.show()
                filename = f'{renyi}-L{L}-a{mask_A}-OBC-n{beta}-Delta{Delta}-I3-vs-{comparison}-comparison.png'
                pyplot.savefig(filename, format='png', dpi=300, transparent=False)
                print("Written", filename)
            con.close()
