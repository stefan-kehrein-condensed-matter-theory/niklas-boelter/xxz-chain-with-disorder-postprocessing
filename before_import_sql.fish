#!/usr/bin/env fish
if test (count $argv) -ne 2;
    echo "Usage:" (status filename) "number_of_spins boundary_conditions" 1>&2;
    exit -1
end
set number_of_spins $argv[1]
set boundary_conditions $argv[2]

echo ".load $HOME/qubit-chain-tfd/cksumvfs.so"
echo ".open /scratch/niklas.boelter/rocks-rawdata/xxz-disorder-random-states-$boundary_conditions-N$number_of_spins.db"
echo ".load $HOME/qubit-chain-tfd/libsqlitefunctions.so"
echo "CREATE TABLE IF NOT EXISTS results (L integer, mask_A integer, mask_C integer, lambda real, V real, mu real, beta real, t real, S_A real, S_B real, S_C real, S_D real, S_AB real, S_AC real, S_AD real, I3 real, Smin_A real, Smin_B real, Smin_C real, Smin_D real, Smin_AB real, Smin_AC real, Smin_AD real, I3_min real, S2_A real, S2_B real, S2_C real, S2_D real, S2_AB real, S2_AC real, S2_AD real, I3_2 real, magnetization real, squared_magnetization real, runtime REAL, version TEXT, zero_magnetization_ipr real, zero_magnetization_participation_Shannon_entropy real, zero_magnetization_participation_collision_entropy real, zero_magnetization_participation_min_entropy real, consecutive_level_spacing_ratio real);"
cat "subsystem_entropies.sql"
