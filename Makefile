CC=gcc
CFLAGS=-O2
CPPFLAGS=
LDFLAGS=-lsqlite3 -lm

ifeq ($(shell uname),Darwin)
	LIBS=libsqlitefunctions.dylib cksumvfs.dylib popcount.dylib
	CPPFLAGS+=-I/usr/local/opt/sqlite/include
	LDFLAGS+=-L/usr/local/opt/sqlite/lib -Wl,-rpath,/usr/local/opt/sqlite/lib
else
	LIBS=libsqlitefunctions.so cksumvfs.so popcount.so
endif

ifeq ($(shell hostname -s),ds10)
	CPPFLAGS+=-I/scratch/AG-Kehrein/sqlite-latest/include
	LDFLAGS+=-L/scratch/AG-Kehrein/sqlite-latest/lib -Wl,-rpath,/scratch/AG-Kehrein/sqlite-latest/lib
endif


default: import libs

.PHONY: libs

libs: $(LIBS)

import: import.c
	$(CC) -o $@ $^ $(CFLAGS) $(CPPFLAGS) $(LDFLAGS)

%$(SUFFIX).so: %.c
	$(CC) $(CFLAGS) $(CPPFLAGS) $(LDFLAGS) -fPIC -shared -o $@ $^

%$(SUFFIX).dylib: %.c
	$(CC) $(CFLAGS) $(CPPFLAGS) $(LDFLAGS) -fno-common -dynamiclib -o $@ $^

.PHONY: clean

clean:
	rm -f import $(LIBS)
