#!/usr/bin/env fish
set tablename 'results_AC_AD_ac27935'
set count 0
for BC in OBC PBC
    for N in 6 8 10 12 14
        if not test -f "/scratch/$USER/rocks-rawdata/xxz-disorder-random-states-$BC-N$N.db"
            $HOME/xxz-chain-with-disorder-postprocessing/before_import_sql_AC_AD.fish $N $BC|sqlite3
        end
        set newdata 0
        for db in /net/theorie/rocks/$USER/qubit-chain-tfd/xxz-$BC-AC-AD-N$N-*.db
            echo "Importing $db.$tablename"
            if test (sqlite3 $db .tables) = $tablename
                $HOME/xxz-chain-with-disorder-postprocessing/import $db $tablename /scratch/$USER/rocks-rawdata/xxz-disorder-random-states-$BC-N$N.db results_AC_AD
                and rm -v $db
                set newdata (math "$newdata+1")
                set count (math $count + 1)
            else
                echo "Skipping $db: Extra tables present" 1>&2
            end
        end
        if test $newdata -gt 0
            $HOME/xxz-chain-with-disorder-postprocessing/after_import_sql_AC_AD.fish $N $BC|sqlite3 -echo
        end
    end
end
set tablename 'results_ac27935'
for BC in OBC PBC
    for N in 6 8 10 12 14
        if not test -f "/scratch/$USER/rocks-rawdata/xxz-disorder-random-states-$BC-N$N.db"
            $HOME/xxz-chain-with-disorder-postprocessing/before_import_sql.fish $N $BC|sqlite3
        end
        set newdata 0
        for db in /net/theorie/rocks/$USER/qubit-chain-tfd/xxz-$BC-N$N-*.db
            echo "Importing $db.$tablename"
            if test (sqlite3 $db .tables) = $tablename
                $HOME/xxz-chain-with-disorder-postprocessing/import $db $tablename /scratch/$USER/rocks-rawdata/xxz-disorder-random-states-$BC-N$N.db results
                and rm -v $db
                set newdata (math "$newdata+1")
                set count (math $count + 1)
            else
                echo "Skipping $db: Extra tables present" 1>&2
            end
        end
        if test $newdata -gt 0
            $HOME/xxz-chain-with-disorder-postprocessing/after_import_sql.fish $N $BC|sqlite3 -echo
        end
    end
end
echo "Imported $count database files!"
rsync -rv ~/scratch/rocks-rawdata/ ~/scratch1/rocks-rawdata --checksum
