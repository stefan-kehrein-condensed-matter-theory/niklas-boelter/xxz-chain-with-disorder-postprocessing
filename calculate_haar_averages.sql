.load ../qubit-chain-tfd/popcount
.load ../qubit-chain-tfd/libsqlitefunctions
.header on
.mode column
SELECT r.L as N, r.mask_A, popcount(r.mask_C) as l_C, r.lambda as Delta, r.V as W, r.mu, r.beta, A.S + B.S + C.S + D.S - avg(AB.S+r.S_AC+r.S_AD) as I3, stdev(r.S_AC + r.S_AD) as I3_std,A.S2 + B.S2 + C.S2 + D.S2 - avg(AB.S2 + r.S2_AC + r.S2_AD) as I3_2, stdev(r.S2_AC + r.S2_AD) as I3_2_std, A.Smin + B.Smin + C.Smin + D.Smin - avg(AB.Smin + r.Smin_AC + r.Smin_AD) as I3_min, stdev(r.Smin_AC + r.Smin_AD) as I3_min_std, COUNT(*) as samplesize FROM results_AC_AD_Haar AS r, subsystem_entropies AS A, subsystem_entropies AS B, subsystem_entropies AS C, subsystem_entropies AS D, subsystem_entropies AS AB WHERE A.N = r.L AND A.l = popcount(r.mask_A) AND B.N = r.L AND B.l = (r.L - popcount(r.mask_A)) AND C.N = r.L AND C.l = popcount(r.mask_C) AND D.N = r.L AND D.l = (r.L - popcount(r.mask_C)) AND AB.N = r.L AND AB.l = r.L GROUP BY r.L, r.mask_A, popcount(r.mask_C), r.lambda, r.V, r.mu, r.beta;
CREATE TABLE IF NOT EXISTS haar_averages (N integer, l integer, Delta real, mu real, beta real, I3, I3_std, I3_2, I3_2_std, I3_min, I3_min_std, samplesize, CONSTRAINT haar_averages_uniq UNIQUE (N,l,Delta,mu,beta));

REPLACE INTO haar_averages SELECT r.L as N, popcount(r.mask_A) as l, r.lambda as Delta, r.mu, r.beta, A.S + B.S + C.S + D.S - avg(AB.S+r.S_AC+r.S_AD) as I3, stdev(r.S_AC + r.S_AD) as I3_std, A.S2 + B.S2 + C.S2 + D.S2 - avg(AB.S2 + r.S2_AC + r.S2_AD) as I3_2, stdev(r.S2_AC + r.S2_AD) as I3_2_std, A.Smin + B.Smin + C.Smin + D.Smin - avg(AB.Smin + r.Smin_AC + r.Smin_AD) as I3_min, stdev(r.Smin_AC + r.Smin_AD) as I3_min_std, COUNT(*) as samplesize FROM results_AC_AD_Haar AS r, subsystem_entropies AS A, subsystem_entropies AS B, subsystem_entropies AS C, subsystem_entropies AS D, subsystem_entropies AS AB WHERE A.N = r.L AND A.l = popcount(r.mask_A) AND B.N = r.L AND B.l = (r.L - popcount(r.mask_A)) AND C.N = r.L AND C.l = popcount(r.mask_C) AND D.N = r.L AND D.l = (r.L - popcount(r.mask_C)) AND AB.N = r.L AND AB.l = r.L AND r.mask_A IN (1, 3, 7, 15, 31, 63, 127, 255, 511) AND popcount(r.mask_A) = popcount(r.mask_C) GROUP BY r.L, r.mask_A, popcount(r.mask_C), r.lambda, r.mu, r.beta;
SELECT * FROM haar_averages ORDER BY l ASC, N ASC;
.output haar_averages.sql
.dump haar_averages
.output
